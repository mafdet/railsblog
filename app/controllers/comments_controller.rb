class CommentsController < ApplicationController
  
  # Basic HTTP Authentication, delete comments
  http_basic_authenticate_with name: "sgoyette", password: "Krsn@108",
  only: :destroy
  
  # Creates comment
  def create
    @article = Article.find(params[:article_id])
    @comment = @article.comments.create(comment_params)
    redirect_to article_path(@article)
  end
  
  def destroy
    @article = Article.find(params[:article_id])
    @comment = @article.comments.find(params[:id])
    @comment.destroy
    redirect_to article_path(@article)
  end
  
  private
    
    def comment_params
      params.require(:comment).permit(:commenter, :body)
    end
  
end
