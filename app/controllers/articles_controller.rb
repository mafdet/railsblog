class ArticlesController < ApplicationController
  
  # Simple HTTP Authentication
  http_basic_authenticate_with name: "sgoyette", password: "Krsn@108",
  except: [:index, :show]
  
  # Displays index of articles
  def index
    @articles = Article.all
  end
  
  # Displays the article
  def show
    @article = Article.find(params[:id])
  end
  
  def new
    @article = Article.new
  end
  
  # Edits the artcile
  def edit
    @article = Article.find(params[:id])
  end
  
  # Creates new article
  def create
    @article = Article.new(article_params)
    
    if @article.save
      redirect_to @article
    else
      render 'new'
    end
  end
  
  def update
    @article = Article.find(params[:id])
    if @article.update(article_params)
      redirect_to @article
    else
      render 'edit'
    end
  end
  
  def destroy
    @article = Article.find(params[:id])
    @article.destroy
    
    redirect_to articles_path
  end
  
  private
    # Article params, what is permitted
    def article_params
      params.require(:article).permit(:title, :text)
    end
  
end
